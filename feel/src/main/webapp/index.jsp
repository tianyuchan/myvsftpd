<%--
  Created by IntelliJ IDEA.
  User: TianyuChen
  Date: 2020/4/5
  Time: 21:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head>
    <title>index</title>
    <link rel="stylesheet" href="kindeditor/themes/default/default.css" />
    <script src="kindeditor/kindeditor-all.js"></script>
    <script src="kindeditor/lang/zh_CN.js"></script>
    <script type="text/javascript">
        KindEditor.ready(function(K) {
            // 文本编辑框
            editor = K.create('textarea[name="feel"]', {
                uploadJson:'upload'
            });

            // 多图片上传
            var editor = K.editor({
                allowFileManager : true,
                uploadJson:'upload'
            });

            K('#J_selectImage').click(function() {
                editor.loadPlugin('multiimage', function() {
                    editor.plugin.multiImageDialog({
                        clickFn : function(urlList) {
                            var div = K('#J_imageView');
                            div.html('');
                            K.each(urlList, function(i, data) {
                                div.append('<img src="' + data.url + '" width="50" height="50" />');
                                div.append('<input type="hidden" name="imgs" value="' + data.url + '" />');
                            });
                            editor.hideDialog();
                        }
                    });
                });
            });
        });
    </script>
</head>
<body>
    <form action="insert" method="post">
        标题：<input type="text" name="title"/><br/>
        图片：<input type="button" id="J_selectImage" value="批量上传" />
        <div id="J_imageView"></div>
        感受：<textarea name="feel" style="width:700px;height:200px;visibility:hidden;">写下你的感受吧。。。</textarea>
        <input type="submit" value="发布"/>
    </form>
</body>
</html>

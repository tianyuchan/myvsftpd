package com.cty.service;

import com.cty.pojo.Feel;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Auther: cty
 * @Date: 2020/4/5 22:35
 * @Description:
 * @version: 1.0
 */
public interface FeelService {

    Map<String, Object> upload(MultipartFile imgFile) throws IOException;

    int insert(Feel feel, List<String> imgs);
}

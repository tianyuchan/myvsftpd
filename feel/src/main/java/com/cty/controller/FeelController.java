package com.cty.controller;

import com.cty.pojo.Feel;
import com.cty.service.FeelService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Auther: cty
 * @Date: 2020/4/5 22:53
 * @Description:
 * @version: 1.0
 */
@Controller
public class FeelController {
    @Resource
    private FeelService feelServiceImpl;

    @RequestMapping("upload")
    @ResponseBody
    public Map<String, Object> upload(MultipartFile imgFile){
        try {
            return this.feelServiceImpl.upload(imgFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping("insert")
    // 返回多个同name参数要使用注解RequestParam
    public String insert(Feel feel, @RequestParam("imgs") List<String> imgs){
        int index = this.feelServiceImpl.insert(feel,imgs);
        if(index==1){
            return "success";
        }else {
            return "error";
        }
    }
}

package com.cty.ftp;

import org.apache.commons.net.ftp.FTPClient;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @Auther: cty
 * @Date: 2020/4/4 22:20
 * @Description:
 * @version: 1.0
 */
public class MyFtpClient {
    public static void main(String[] args) throws IOException {
        FTPClient ftp = new FTPClient();
        // 设置IP和端口（一定写在用户名和密码上面）
        ftp.connect("192.168.238.128", 21);
        // 设置用户名和密码
        ftp.login("ftpuser", "ftpuser");
        // 设置文件类型
        ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
        // 选择上传文件
        InputStream is = new FileInputStream("E:/xiaer.png");
        // 切换目录（默认在/home/ftpuser/）
        ftp.changeWorkingDirectory("/home/ftpuser/test/");
        // 上传（param1：存储时的参数名 param2：文件）
        ftp.storeFile("32.png", is);
        // 退出
        is.close();
        if(ftp.logout()){
            System.out.println("上传成功");
        }else {
            System.out.println("上传失败");
        }
    }
}

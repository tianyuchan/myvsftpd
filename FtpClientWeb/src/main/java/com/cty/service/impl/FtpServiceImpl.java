package com.cty.service.impl;

import com.cty.service.FtpService;
import com.cty.utils.FtpUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

/**
 * @Auther: cty
 * @Date: 2020/4/5 11:47
 * @Description:
 * @version: 1.0
 */
@Service
public class FtpServiceImpl implements FtpService {
    Logger log = Logger.getLogger(FtpServiceImpl.class);

    @Value("${ftpclient.host}")
    private String host;
    @Value("${ftpclient.port}")
    private int port;
    @Value("${ftpclient.username}")
    private String username;
    @Value("${ftpclient.password}")
    private String password;
    @Value("${ftpclient.basePath}")
    private String basePath;
    @Value("${ftpclient.filepath}")
    private String filePath;

    public boolean upload(MultipartFile file) throws IOException {
        String fileName = UUID.randomUUID()+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        log.info("将上传文件："+file.getOriginalFilename());
        return FtpUtil.uploadFile(host, port, username, password, basePath, filePath, fileName, file.getInputStream());
    }
}

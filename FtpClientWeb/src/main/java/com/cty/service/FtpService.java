package com.cty.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @Auther: cty
 * @Date: 2020/4/5 11:39
 * @Description:
 * @version: 1.0
 */
public interface FtpService {
    boolean upload(MultipartFile file) throws IOException;
}

package com.cty.controller;

import com.cty.service.FtpService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @Auther: cty
 * @Date: 2020/4/5 12:01
 * @Description:
 * @version: 1.0
 */
@Controller
public class FtpController {
    @Resource
    private FtpService ftpServiceImpl;

    @RequestMapping("upload")
    public String upload(MultipartFile file) throws IOException {
        this.ftpServiceImpl.upload(file);
        return "success";
    }

}

# vsftpd

#### 介绍
vsftpd+nginx+FTPCilent

#### 软件架构
FtpClinent
* jar项目，使用FTPClient上传文件到vsftpd服务器

FtpClinentWeb
* war项目，浏览器选择文件上传到vsftpd服务器

generatorSqlmapCustom
* 该项目在generatorConfig.xml中指定数据库和数据表后，自动生成Mapper和pojo文件

feel
* 动态发布（包括标题、图片、文本），只写了存储功能，没写展示功能
* 使用了generatorSqlmapCustom项目生成Mapper和pojo文件
* 图片批量上传和文本编辑使用了前端框架kindeditor
* 只能使用Chrome浏览器

#### 运行环境

1.  jdk-13
2.  Intellij IDEA 2019.3
3.  apache-tomcat-9.0.30
4.  apache-maven-3.6.3
5.  mysql-5.5.60(feel)
6.  vsftpd(feel)
7.  nginx-1.8.0(feel)

#### 结果展示
feel项目结果展示

* index页面

![index页面](picture/index页面.png)

* 编辑

![编辑](picture/编辑.png)

* 上传成功

![上传成功](picture/上传成功.png)


#### 数据库设计

1. 使用数据库：MySQL
2. 数据库可视化软件：Navicat
3. 数据库名：ssm
4. feel项目feel表设计和数据

![feel表设计和数据](picture/feel表设计和数据.png)

5 . feel项目img表设计和数据

![img表设计和数据](picture/img表设计和数据.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
